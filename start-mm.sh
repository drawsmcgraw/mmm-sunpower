#!/bin/sh

docker run  --rm \
    --publish 8080:8080 \
    --volume ${PWD}/modules:/opt/magic_mirror/modules \
    --volume ${PWD}/config:/opt/magic_mirror/config \
    --volume /etc/localtime:/etc/localtime:ro \
    --name magic_mirror \
    bastilimbach/docker-magicmirror



