
# MMM-Sunpower
Visualize your Sunpower PV system on your MagicMirror!

![sample](./mmm-sunpower.png)

# THIS IS THE DEVELOPMENT REPO
This repo is used for development. If you're looking to install/use this module, go to [MMM-Sunpower-Monitor](https://gitlab.com/drawsmcgraw/MMM-Sunpower-Monitor) and install that.

## How it works

As of this writing, Sunpower doesn't have a publically accessible API. However, the monitoring appliance that sends the data to their servers exposes a simple REST interface.

This module scrapes the API of the Sunpower monitoring appliance in your home every ten seconds to fetch how much power is currently being generated, then displays that on a gauge.

## Setting up access to your appliance

Most setups install the monitoring appliance outside the house, making it a challenge to establish a connection to the appliance. You can use a Raspberry Pi, with haproxy, to act as a wireless bridge to provide this connection. See the PDF in this repo for instructions, as well as more detailed information on the appliance's API.

NOTE: I am not the author of the PDF.

## Installation

Clone this module into your MagicMirror’s modules directory, run `npm install` in the module’s directory:

```bash
git clone https://github.com/drawsmcgraw/MMM-sunpower
cd MMM-sunpower
npm install
```

## Updating

Updating is as easy as navigating to the module’s folder, pull the latest version from GitHub and install.

```bash
git pull
npm install
```


## Configuration

Sample config for copy/pasting:

```json
{   module: "MMM-Sunpower",
    position: "top_right",
	config: {
		// Capacity, in Kilowatts, of your solar PV system.
		capacity: 8.64,

		// Path to your Sunpower appliance.
		// Must include 'http://'
		sunpowerHostname: "http://sunpower-appliance.home.net"
	  }
},
```

| Option | Description |
| ------ | ----------- |
| `capacity` | The size of your solar PV system, in kilowatts. <br>Example: 8.6|
| `sunpowerHostname` | A valid IP or DNS name pointing to your Sunpower appliance on your home network. <br>Example: `sunpower-appliance.home.net` |

## More Resources

A lot of work has been done in this area. Further reading/collaboration can be found at the following links:

https://github.com/ginoledesma/sunpower-pvs-exporter

https://nelsonslog.wordpress.com/2021/12/02/getting-local-access-to-sunpower-pvs6-data/

https://blog.gruby.com/2020/04/28/monitoring-a-sunpower-solar-system/comment-page-1/

https://github.com/krbaker/sunpower/

https://github.com/jeffkowalski/sunpower
