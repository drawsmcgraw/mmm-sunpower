Module.register("MMM-Sunpower", {
  getHeader: function() {
    return "Solar Panel Output";
  },

  getScripts: function () {
    return ['https://cdn.plot.ly/plotly-2.20.0.min.js' ];
  },

  defaults: {},
  start: function () {
    this.sendSocketNotification("CONFIG", this.config);
    var self = this
  },
  

  getDom: function() {
    var element = document.createElement("div")
    element.innerHTML = "Current Power Generation"

    //var totalkw = document.createElement("p")
    //totalkw.innerHTML = "Fetching current power..."
    //totalkw.id = "COUNT"

    //var totalkwh = document.createElement("p")
    //totalkwh.innerHTML = "Fetching total power..."
    //totalkwh.id = "totalkwh"

    var gaugeDiv = document.createElement("div");
    gaugeDiv.id = "my-gauge";

    element.appendChild(gaugeDiv);
    //element.appendChild(totalkw)
    //element.appendChild(totalkwh)
    return element
  },

  socketNotificationReceived: function(notification, payload) {
    switch(notification) {
      case "sunpower-data-fetched":

        // Calculate 1/3 of total capacity so we can dynamically
        // generate three "sections" on the gauge.
        segment = parseFloat((this.config.capacity / 3.0).toFixed(2))
        
        // How large our black "border" should be between segments
        borderOffset = parseFloat(0.1)

        var data = [
          {
            domain: { x: [0, 1], y: [0, 1] },
            value: payload.totalkw,
            title: { text: "Kilowatts" },
            type: "indicator",
            mode: "gauge+number",
            gauge: {
              axis: { range: [0, this.config.capacity], tickwidth: 1, tickcolor: "darkblue" },
              bar: { color: "darkblue" },
              bgcolor: "black",
              borderwidth: 2,
              bordercolor: "gray",
              steps: [
                { range: [0, segment], color:   "red" },
                { range: [segment, segment + borderOffset], color: "black" },
                { range: [segment + borderOffset, segment*2], color: "yellow" },
                { range: [segment*2, (segment*2) + borderOffset], color: "black" },
                { range: [(segment*2) + borderOffset, this.config.capacity], color: "green" }
              ],
            }
          }
        ];

        var layout = { 
          width: 300, 
          height: 250, 
          margin: { t: 0, b: 0 },
          paper_bgcolor: "black",
          font: { color: "white", family: "Arial" }
        };
        Plotly.newPlot('my-gauge', data, layout);
        break
    }
  },
   
  notificationReceived: function(notification, payload, sender) {
    switch(notification) {
      case "DOM_OBJECTS_CREATED":
        // run every 10s (or 10000ms)
        console.log("Sending sunpower-fetch-data");
        console.log("Config is " + this.config.sunpowerHostname);
        var timer = setInterval(() => {
          this.sendSocketNotification("sunpower-fetch-data", this.config)
        }, 10000) // 10000 = 10 seconds
        break
    }
  },
  
})